#include "rzwboom.h"
#include <vector>
#include <iostream>

using namespace std;


int main() {
	std::vector<int> boomStructuur = { 10, 3, 2, 6, 4, 8, 15, 12, 11, 13, 18, 16, 20 };
	std::vector<int> boomKleuren = { 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0 };

	RZWboom<int> boompje(boomStructuur, boomKleuren);
//    boompje.tekenAlsBinaireBoom("boom.txt");
//    boompje.repOK();
    boompje.repOKZoekboom();
}
